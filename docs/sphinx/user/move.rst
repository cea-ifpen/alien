.. _user_move_build:

Building with move semantic
===========================

.. doxygenclass:: Alien::DirectMatrixBuilder
   :project: doxygen_api_docs_movesemantic
   :members:

.. doxygenclass:: Alien::ProfiledMatrixBuilder
   :project: doxygen_api_docs_movesemantic

.. doxygenclass:: Alien::VectorReader
   :project: doxygen_api_docs_movesemantic

.. doxygenclass:: Alien::VectorWriter
   :project: doxygen_api_docs_movesemantic
